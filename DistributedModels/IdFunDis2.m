function [tuM,tdM,auM,adM]=IdFunDis2(q,n,B,m,Sb,Y0,L, NodeNumGiven,sv)

%Based on the common sense and experimetns, the upstream and the dowsntream
%direction backwater area is the same. And it should be the sum of the
%individual ones

%The time delays are calculated for each part then added up

% clear 
% close all
addpath('Final')
% tic
% %The physical data of the reach
% 
NodeNumGiven=31;
% 
Sb=0.0015;
L=3000;
B=3;
m=0;
n=0.03;
Y0=3;
q=1.5;
sv=0;
% Sb=0.001;
% Y0=1;

%sv=0;
% switch nargin
%     case 8
%         sv=0;
% end

g=9.81;

if sv==1
Distance=L/(NodeNumGiven-2);
NumberHPoints=(NodeNumGiven-2)/2-1;
else
Distance=L/(NodeNumGiven-1);
NumberHPoints=(NodeNumGiven-1);    
end
%Work on this!
%The number of the parts is nodenum*2-2
%I want 6 parts, 600 parts
[ HinitH, distance] = SteadyStateNotSV(L, m,B,n,Sb,q,Y0,NodeNumGiven, 0,10);
Startexp=-4.5;
Finexp=0;
TimePoints=0;
SpacePoints=500;

%%
% This loop caclulates the parameters for all water level points in a reach
for k=1:NumberHPoints
%for k=1:3
    if sv==1
    x2=Distance+Distance*2*(k-1); %x2 is the horizontal distance from upstream of the chosen point
    else
    x2=Distance*(k-1);
    end

    %Stores in matrix the longitudinal distance and the corresponding water
    x2M(k)=x2;
 %   y2=HinitH(699-(k-1)*100);
    y2=HinitH(k);

    y2M(k)=y2; %good
    %Note: this can be better by real backwater
    if k>1 
    %This is the transfer function from upstream to a certain point
    %That is why we are taking the corresponding water depth (that we
    %calculate) and the length that is from upstream to that point (x2)
    %For p21 inf and taud Up, the upstream part
    [freq, p21_mag_dist, p22_mag_dist, p11_mag_dist, p12_mag_dist, appra11, appra12, appra21, appra22 ]=...
    BackwaterAreaApproximationFast(m, B, Sb, n,q, x2, y2, Startexp, Finexp, TimePoints,SpacePoints);
    adM(k)=appra21(end);
    
   
    [tu td tau]=TimeDelayFun(q,n,B,m,Sb,y2,x2); %
    %----------------------------------------------------------------------
    %These are the effects from downstream to a certain point
    %[p11_inf_, p12_inf_ , ~, ~, au_, ~, tu_ , ~, ~]=IdzFun(q,n,B,m,Sb,Y0,L-x2);  %Think about this claim for later for the distributed
     [freq, p21_mag_dist, p22_mag_dist, p11_mag_dist, p12_mag_dist, appra11, appra12, appra21, appra22 ]=...
    BackwaterAreaApproximationFast(m, B, Sb, n,q, L-x2, Y0, Startexp, Finexp, TimePoints,SpacePoints);
    auM(k)=appra12(end);
    [tu_ td_ tau_]=TimeDelayFun(q,n,B,m,Sb,Y0,L-x2); %
    tuM(k)=tu+tu_;
    tdM(k)=td+td_;
    end
end
%%
 [freq, p21_mag_dist, p22_mag_dist, p11_mag_dist, p12_mag_dist, appra11o, appra12o, appra21o, appra22o ]=...
    BackwaterAreaApproximationFast(m, B, Sb, n,q, L, Y0, Startexp, Finexp, TimePoints,SpacePoints);
auM(1)=appra12o(end);
adM(1)=0;
auM(NumberHPoints+1)=0;
adM(NumberHPoints+1)=appra21o(end);

