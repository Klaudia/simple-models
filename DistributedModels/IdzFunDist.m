function [tuM,tdM,pum,pdm,puma,x2M]=IdzFunDist(q,n,B,m,Sb,Y0,L, NodeNumGiven,sv)

%clear 
%close all
addpath('Final')

%The physical data of the reach

% NodeNumGiven=30;
% 
% Sb=0.0015;
% L=3000;
% B=3;
% m=0;
% n=0.03;
% Y0=3;
% q=1.5;

switch nargin
    case 8
        sv=0;
end

if sv==1
Distance=L/(NodeNumGiven-2);
NumberHPoints=(NodeNumGiven-2)/2-1;
else
Distance=L/(NodeNumGiven-1);
NumberHPoints=(NodeNumGiven-1);    
end

sxx=(Sb-sf0(Y0,q,n,B,m))/(1-(froude(Y0,q,n,B,m))^2);

%Original model
[p11_info, p12_info , p21_info, p22_info, ~, ~, ~, ~, yn,~]=IdzFun(q,n,B,m,Sb,Y0,L);

% This loop caclulates the parameters for all water level points in a reach
for k=1:NumberHPoints+1
    if sv==1
    x2=Distance+Distance*2*(k-1); %x2 is the horizontal distance from upstream of the chosen point
    else
    x2=Distance*(k-1);
    end
    y2=Y0-(L-x2)*sxx; %y2 is the water level belonging to x2
    if y2<yn
        y2=yn;
    end
    %Stores in matrix the longitudinal distance and the corresponding water
    x2M(k)=x2;
    y2M(k)=y2; %good
    %Note: this can be better by real backwater
    
    %This is the transfer function from upstream to a certain point
    %That is why we are taking the corresponding water depth (that we
    %calculate) and the length that is from upstream to that point (x2)
    %For p21 inf and taud Up, the upstream part
    [~, ~ , p21_inf, p22_inf, ~, ad, ~, td , ~]=IdzFun(q,n,B,m,Sb,y2,x2);
    tdM(k)=td;
    %tdMD(k)=floor(td/dt); %the discretized parameters
    %----------------------------------------------------------------------
    %These are the effects from downstream to a certain point
    [p11_inf_, p12_inf_ , ~, ~, au_, ~, tu_ , ~, ~]=IdzFun(q,n,B,m,Sb,Y0,L-x2);  %Think about this claim for later for the distributed
    tuM(k)=tu_;
    %tuMD(k)=floor(tu_/dt);
    %These formulas are deduced by hand from Freq. appr.
    pum(k)=p21_inf-(p21_inf*p22_inf)/(p11_inf_+p22_inf);
    pdm(k)=-(p12_inf_*p22_inf)/(p11_inf_+p22_inf);
    puma(k)=ad+au_;
end
pdm(1)=p12_info;
pdm(end)=-p22_info;

pum(1)=p11_info;
pum(end)=p21_info;
