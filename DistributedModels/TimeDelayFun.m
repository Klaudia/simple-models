%Time delay 
function [tu, td, tau]=TimeDelayFun(q,n,B,m,Sb,Y0,L)
b0=B;
y2=Y0;
 a=b0*y2+m*y2^2;
 g=9.81;
% p=b0+2*y2*(1+m^2)^0.5;
% r=a/p;
 t=b0+2*y2*m;
 c=(g*a/t)^0.5;
 v=q/a;
 tu=L/(c+v);
 td=L/(c-v);
 tau=L/(c+v)/2+L/(c-v)/2;