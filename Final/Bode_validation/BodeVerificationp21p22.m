%Verification Bode for p21 and p22
%Litrico canal1
close all
clear

litfricp21phase=csvread('Data\fric\p21_phase_fric.csv');
litfricp21mag=csvread('Data\fric\p21_mag_fric.csv');
litfricp22phase=csvread('Data\fric\p22_phase_fric.csv');
litfricp22mag=csvread('Data\fric\p22_mag_fric.csv');

x=3000;
m=1.5;
B=7;
Sb=1e-4;
%Sb=0;
n=0.02;
%n=0;
Y0=2.12;
q=14;

 g=9.81;
Startexp=-3.5;
Finexp=-1.5;
TimePoints=1000;
SpacePoints=600;   %Number of discretization points%The choice will influence the distance

%-----------------------------------------------------------------

cd ..
             
[freq, p21_mag, p21_phase_corr, p22_mag, p22_phase_corr, p11_mag, p11_phase_corr, p12_mag, p12_phase_corr ]=...
    Bode_fun(m, B, Sb, n,q, x, Y0, Startexp, Finexp, TimePoints,SpacePoints);             

cd Bode_validation

%%
close all
%load litfric3

figure(6)
subplot(2,1,1)

my_width=1;
semilogx(litfricp21mag(:,1),litfricp21mag(:,2),'r','LineWidth',my_width)
hold on
semilogx(freq,p21_mag,'b','LineWidth',my_width)
grid
xlabel('freq. (rad/s)')
ylabel('gain (dB)')
xlim([min(freq) max(freq)])

title('p21')

subplot(2,1,2)
my_width=1;
semilogx(litfricp21phase(:,1),litfricp21phase(:,2),'r','LineWidth',my_width)
hold on
semilogx(freq,p21_phase_corr,'b','LineWidth',my_width)


grid
xlabel('freq. (rad/s)')
ylabel('phase (dg)')
xlim([min(freq) max(freq)])


figure(8)
subplot(2,1,1)

my_width=1;
semilogx(litfricp22mag(:,1),litfricp22mag(:,2),'r','LineWidth',my_width)

hold on
semilogx(freq,p22_mag,'b','LineWidth',my_width)
title('p22')
grid
xlabel('freq. (rad/s)')
ylabel('gain (dB)')
xlim([min(freq) max(freq)])

subplot(2,1,2)
my_width=1;
semilogx(litfricp22phase(:,1),litfricp22phase(:,2),'r','LineWidth',my_width)

hold on
semilogx(freq,p22_phase_corr,'b','LineWidth',my_width)


grid
xlabel('freq. (rad/s)')
ylabel('phase (dg)')
xlim([min(freq) max(freq)])

