%Read data IDZ
clear all
%addpath('D:\Klaudia\GitRepos\SimpleModels');
table1=xlsread('LitricoData.xlsx','Table1');
table2=xlsread('LitricoData.xlsx','Table2');
table4=xlsread('LitricoData.xlsx','Table4');
table5=xlsread('LitricoData.xlsx','Table5');
table6=xlsread('LitricoData.xlsx','Table6');
table7=xlsread('LitricoData.xlsx','Table7');

cd ..

n1=0.014;
m1=1.5;
m2=m1;
n2=0.02;
sb1=0.002;
sb2=0.0001;

bm=table1(:,3);
lm=table1(:,2);
hm=table1(:,4);

qm=table4(:,2);
td=table4(:,4);
pool_number=table4(:,1);
ad=table4(:,6);
p21_inf=table4(:,8);
p22_inf=table4(:,10);

pool_all=length(pool_number);
nm=ones(pool_all,1)*n1;
sbm=ones(pool_all,1)*sb1;
mm=ones(pool_all,1)*m1;



for k=1:pool_all
[p11_inf_hat, p12_inf_hat, p21_inf_hat, p22_inf_hat, au_hat, ad_hat, tu_hat, td_hat, yn, x2]=...
    IdzFun(qm(k),nm(k),bm(k),mm(k),sbm(k),hm(k),lm(k));
ad_cal(k)=ad_hat;
td_cal(k)=td_hat;
p21_inf_cal(k)=p21_inf_hat;
p22_inf_cal(k)=p22_inf_hat;
end

error_table4=zeros(pool_all,4);

for k=1:pool_all
    if abs(round(ad_cal(k)',1)-ad(k))>0.1
       error_table4(k,2)=1; 
    end
if abs(round(td_cal(k)',1)-td(k)) >0.1
    error_table4(k,1)=1;
end
if abs(round(p22_inf_cal(k)',4)-p22_inf(k))>0.00005
    error_table4(k,4)=1;
end
if abs(round(p21_inf_cal(k)',4)-p21_inf(k))>0.00005
    error_table4(k,3)=1;
end
end

%table5---------------------------------------------------------------

%floor(ad_cal'*10)/10-ad
qm=table5(:,2);
td=table5(:,4);
pool_number=table5(:,1);
ad=table5(:,6);
p21_inf=table5(:,8);
p22_inf=table5(:,10);

pool_all=length(pool_number);
nm=ones(pool_all,1)*n1;
sbm=ones(pool_all,1)*sb1;
mm=ones(pool_all,1)*m1;



for k=1:pool_all
[p11_inf_hat, p12_inf_hat, p21_inf_hat, p22_inf_hat, au_hat, ad_hat, tu_hat, td_hat, yn, x2]=...
    IdzFun(qm(k),nm(k),bm(k),mm(k),sbm(k),hm(k),lm(k));
ad_cal(k)=ad_hat;
td_cal(k)=td_hat;
p21_inf_cal(k)=p21_inf_hat;
p22_inf_cal(k)=p22_inf_hat;
end

error_table5=zeros(pool_all,4);

for k=1:pool_all
    if abs(round(ad_cal(k)',1)-ad(k))>0.1
       error_table5(k,2)=1; 
    end
if abs(round(td_cal(k)',1)-td(k)) >0.1
    error_table5(k,1)=1;
end
if abs(round(p22_inf_cal(k)',4)-p22_inf(k))>0.00005
    error_table5(k,4)=1;
end
if abs(round(p21_inf_cal(k)',4)-p21_inf(k))>0.00005
    error_table5(k,3)=1;
end
end


%table 6-------------------------------------------------------------------
bm=table2(:,3);
lm=table2(:,2);
hm=table2(:,4);

qm=table6(:,2);
td=table6(:,4);
pool_number=table6(:,1);
ad=table6(:,6);
p21_inf=table6(:,8);
p22_inf=table6(:,10);

pool_all=length(pool_number);
nm=ones(pool_all,1)*n2;
sbm=ones(pool_all,1)*sb2;
mm=ones(pool_all,1)*m2;



for k=1:pool_all
[p11_inf_hat, p12_inf_hat, p21_inf_hat, p22_inf_hat, au_hat, ad_hat, tu_hat, td_hat, yn, x2]=...
    IdzFun(qm(k),nm(k),bm(k),mm(k),sbm(k),hm(k),lm(k));
ad_cal(k)=ad_hat;
td_cal(k)=td_hat;
p21_inf_cal(k)=p21_inf_hat;
p22_inf_cal(k)=p22_inf_hat;
end

error_table6=zeros(pool_all,4);

for k=1:pool_all
    if abs(round(ad_cal(k)',0)-ad(k))>0.1
       error_table6(k,2)=1; 
    end
if abs(round(td_cal(k)',1)-td(k)) >0.1
    error_table6(k,1)=1;
end
if abs(round(p22_inf_cal(k)',4)-p22_inf(k))>0.00005
    error_table6(k,4)=1;
end
if abs(round(p21_inf_cal(k)',4)-p21_inf(k))>0.00005
    error_table6(k,3)=1;
end
end

%table 7-------------------------------------------------------------------
bm=table2(:,3);
lm=table2(:,2);
hm=table2(:,4);

qm=table7(:,2);
td=table7(:,4);
pool_number=table7(:,1);
ad=table7(:,6);
p21_inf=table7(:,8);
p22_inf=table7(:,10);

pool_all=length(pool_number);
nm=ones(pool_all,1)*n2;
sbm=ones(pool_all,1)*sb2;
mm=ones(pool_all,1)*m2;



for k=1:pool_all
[p11_inf_hat, p12_inf_hat, p21_inf_hat, p22_inf_hat, au_hat, ad_hat, tu_hat, td_hat, yn, x2]=...
    IdzFun(qm(k),nm(k),bm(k),mm(k),sbm(k),hm(k),lm(k));
ad_cal(k)=ad_hat;
td_cal(k)=td_hat;
p21_inf_cal(k)=p21_inf_hat;
p22_inf_cal(k)=p22_inf_hat;
end

error_table7=zeros(pool_all,4);

for k=1:pool_all
    if abs(round(ad_cal(k)',0)-ad(k))>0.1
       error_table7(k,2)=1; 
    end
if abs(round(td_cal(k)',1)-td(k)) >0.1
    error_table7(k,1)=1;
end
if abs(round(p22_inf_cal(k)',4)-p22_inf(k))>0.00005
    error_table7(k,4)=1;
end
if abs(round(p21_inf_cal(k)',4)-p21_inf(k))>0.00005
    error_table7(k,3)=1;
end
end

