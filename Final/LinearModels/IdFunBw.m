function [ae tau]=IdFunBw(q,n,B,m,Sb,Y0,L,kh)

%This is the implementation of the Integrator Delay model
%More information: 
%Schuurmans, Jan, et al. "Modeling of irrigation and drainage canals for controller design." 
%Journal of irrigation and drainage engineering 125.6 (1999): 338-344.

%Calculationg the divison points in case of other canals-------------------

g=9.81;

kh=0;       %initial height = 0
sb=Sb;      %bottom slope
Man=n;      %Manning coefficient??
b0=B;       %Bottom width
h=Y0;       %initial height??
yx=h;       %downstream height
g=9.81;

%Calculationg the divison points in case of other canals-------------------

% Calculating the normal depth

    %Normal depth
    
    y1=0;                                   %We suppose it is between 0 and 5 times the original depth
    y2=yx*5;        
    for k=1:25          
      y=(y1+y2)/2;                          %first guess (middle of the range)
      a=b0*y+m*y^2;                         %using average height to calculate area     
      p=b0+2*y*(1+m^2)^0.5;                 %wet perimeter calculated trapezoidal cross section
      r=a/p;                                %hydrolic radius
      dif=(q^2*n^2)/(a^2*r^(4/3))-sb;       %friction slope - bottom slope
      if dif<0                              % if bottom slope > friction slope, we are under normal depth
          y2=(y1+y2)/2;
      else                                  % if bottom slope < friction slope, we are over normal depth, lower range is moved to half
          y1=(y1+y2)/2;
      end
    end
    yn=y;                                   %Normal depth


     %Angle between the bottom line and the water surface
     sxx=(sb-sf0(yx,q,n,b0,m))/(1-(froude(yx,q,n,b0,m))^2); 
     %yu=yx-(L)*sxx; %good

    if abs(sxx)<1e-8  %If the angle is zero, normal depth
        x1_calc=L;    %The end of the normal depth zone is in the end of the channel (no bw)
    else
        x1_calc=max(0,L-(yx-yn)/sxx); %Calculate the point where normal depth is reached
    end
    x1=x1_calc;
    %Calculating the corresponding water level
    if x1==0  
        y1=yx-sxx*L;
    else
        y1=yn;
    end



Lbw=L-x1;  %Length of the backwater part
Lunif=x1;  %Length of the uniform part



% Part 3 ----Time delay and backwater area---------------------------------
 y2=yx; %This is the (downstream) water depth we use for calculation.  (should it be the middle...?)
 a=b0*y2+m*y2^2;  %cross sectional area
 t=b0+2*y2*m;     %top width
 c=(g*a/t)^0.5;   %celerity
 v=q/a;           %velocity
 tau=Lunif/(c+v)/2+Lunif/(c-v)/2;  %time delay, uniform part only
 ae=Lbw*t;                         %backwater surface (bw area only)
 