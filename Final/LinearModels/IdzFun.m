function [p11_inf_hat, p12_inf_hat, p21_inf_hat, p22_inf_hat, au_hat, ad_hat, tu_hat, td_hat, yn, x2]=IdzFun(q,n,B,m,Sb,Y0,L)

kh=0;       %initial height = 0
sb=Sb;      %bottom slope
Man=n;      %Manning coefficient??
b0=B;       %Bottom width
h=Y0;       %initial height??
yx=h;       %downstream height

%Calculationg the divison points in case of other canals-------------------

g=9.81;

    %Normal depth
    
    y1=0;           %initial values??
    y2=yx*5;        
    k=0;
    for k=1:25          
      y=(y1+y2)/2;      %average height
      a=b0*y+m*y^2;     %using average height to calculate area     
      p=b0+2*y*(1+m^2)^0.5;     %wet perimeter calculated trapezoidal cross section
      r=a/p;                    %hydrolic radius
      dif=(q^2*n^2)/(a^2*r^(4/3))-sb;       %partial derivative of the friction slope?? sb = bottom slope
      if dif<0
          y2=(y1+y2)/2;
      else
          y1=(y1+y2)/2;
      end
    end
    yn=y;       %Normal depth


% x1=0;
% x2=(L+x1)/2;
% sxx=(sb-sf0(yx,q,n,b0,m))/(1-(froude(yx,q,n,b0,m))^2);
% y2=yx-(L-x2)*sxx; %good


     sxx=(sb-sf0(yx,q,n,b0,m))/(1-(froude(yx,q,n,b0,m))^2); 
     yu=yx-(L)*sxx; %good

    if sxx==0
        x1_calc=L;
    else
        x1_calc=max(0,L-(yx-yn)/sxx);
    end
    x1=x1_calc;
    %y1
    if x1==0
        y1=yx-sxx*L;
    else
        y1=yn;
    end
    %x2
    x2=(L+x1)/2;
    %y2
    %y2=y1+(L-x1)*sb;
    %For backwater
    
    if x1==0
       y2=yx-(L-x2)*sxx;
    else
      y2=yn+(x2-x1)*sxx;
    end
    
    

    sxu=0;

% Part 2 ----- Upstream part - calculation of basic variables--------------

a=b0*y1+m*y1^2;

p=b0+2*y1*(1+m^2)^0.5;

t=b0+2*y1*m;
c=(g*a/t)^0.5;
v=q/a;
cu=c;
vu=v;

sf=sf0(y1,q,n,b0,m);
fr=froude(y1,q,n,b0,m);


kappa=7/3-4*a/(3*t*p)*2*(1+m^2)^0.5;
alpha=(t*(2+(kappa-1)*fr^2)*sb)/(a*fr*(1-fr^2));
beta=-2*g/v*(sb-sxu);       
gamma=g*t*((1+kappa)*sb-(1+kappa-fr^2*(kappa-2))*sxu);

%Low frequencies
x=x1;
td=x/(c+v);
tu=x/(c-v);
ad=(t^2*(c^2-v^2))/(gamma)*(1-exp(-gamma/(t*(c^2-v^2))*x));
au=(t^2*(c^2-v^2))/(gamma)*(exp(gamma/(t*(c^2-v^2))*x)-1);

%High frequencies
p11_inf=1/(t*c*(1-fr))*((1+((1-fr)/(1+fr))^2*exp(alpha*x))/(1+exp(alpha*x)))^0.5;
p12_inf=2/(t*c*(1-fr^2))*((exp(-(gamma)/(2*t*(c^2-v^2))*x))/((1+exp(alpha*x))^0.5));
p21_inf=2/(t*c*(1-fr^2))*((exp((gamma)/(2*t*(c^2-v^2))*x))/((1+exp(alpha*x))^0.5));
p22_inf=1/(t*c*(1+fr))*((1+((1+fr)/(1-fr))^2*exp(alpha*x))/(1+exp(alpha*x)))^0.5;


  
%%

% Part 3 ----Downstream part------------------------------------------------

a=b0*y2+m*y2^2;
p=b0+2*y2*(1+m^2)^0.5;
r=a/p;
t=b0+2*y2*m;

c=(g*a/t)^0.5;
v=q/a;
fr=v/c;
sf=(q^2*n^2)/(a^2*r^(4/3));


 sxd=(sb-sf0(yx,q,n,b0,m))/(1-(froude(yx,q,n,b0,m))^2);

dtdy=2*m;
dtdx=dtdy*sxd;

x=L-x1;

kappa=7/3-4*a/(3*t*p)*2*(1+m^2)^0.5;
alpha=t/(a*fr*(1-fr^2))*((2+(kappa-1)*fr^2)*sb-(2+(kappa-1)*fr^2-(a/t^2*dtdy+kappa-2)*fr^4)*sxx);
gamma1=g*t*((1+kappa)*sb-(1+kappa-fr^2*(kappa-2))*sxd); %paper
gamma=v^2*2*m*sxd+g*t*((1+kappa)*sb-(1+kappa-fr^2*(kappa-2))*sxd); %paper tsst

%Low frequencies

td_=x/(c+v);
tu_=x/(c-v);


ad_=(t^2*(c^2-v^2))/(gamma1)*(1-exp(-(gamma1)/(t*(c^2-v^2))*x));
au_=(t^2*(c^2-v^2))/(gamma1)*(exp((gamma1)/(t*(c^2-v^2))*x)-1);



%High frequencies
p11_inf_=1/(t*c*(1-fr))*((1+((1-fr)/(1+fr))^2*exp(alpha*x))/(1+exp(alpha*x)))^0.5;
p12_inf_=2/(t*c*(1-fr^2))*((exp(-(gamma)/(2*t*(c^2-v^2))*x))/((1+exp(alpha*x))^0.5));
p21_inf_=2/(t*c*(1-fr^2))*((exp((gamma)/(2*t*(c^2-v^2))*x))/((1+exp(alpha*x))^0.5));
p22_inf_=1/(t*c*(1+fr))*((1+((1+fr)/(1-fr))^2*exp(alpha*x))/(1+exp(alpha*x)))^0.5;

% Part 4 -------------------Global model-----------------------------------
 if x1==0
 td_hat=td_;
 tu_hat=tu_;
 
 ad_hat=ad_;%*(1+(ad/au_));
 au_hat=au_;%*(1+(au_/ad));
 
 p11_inf_hat=p11_inf_;
 p12_inf_hat=p12_inf_;
 p21_inf_hat=p21_inf_;
 p22_inf_hat=p22_inf_;     
 else
 td_hat=td+td_;
 tu_hat=tu+tu_;
 
 ad_hat=ad_*(1+(ad/au_));
 au_hat=au*(1+(au_/ad));
 
 p11_inf_hat=p11_inf+(p12_inf*p21_inf)/(p11_inf_+p22_inf);
 p12_inf_hat=(p12_inf*p12_inf_)/(p11_inf_+p22_inf);
 p21_inf_hat=(p21_inf*p21_inf_)/(p11_inf_+p22_inf);
 p22_inf_hat=p22_inf_+(p12_inf_*p21_inf_)/(p11_inf_+p22_inf);
 end
     

 