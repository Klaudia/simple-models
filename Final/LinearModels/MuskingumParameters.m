function [x, K] = MuskingumParameters(L, B, Sb, m, Y0, q)

%from: Cunge, J. A. (1969). On the subject of a flood propagation computation 
% method (Musklngum method). Journal of Hydraulic Research, 7(2), 205-230.


% L  %Length of the river
% B  bottom width
% Sb  %bottom slope
% m  side slope
% Y0  (downstream) water level
% q = 1 discharge

T=B+m*Y0*2;         %Top width
     
g=9.81;             % [m/s^2]
a=B*Y0+m*Y0^2;      %flow area canal [m^2]
c=(g*a/B)^0.5;      %velocity of the wave [m/s]
v=q/a;              %flow velocity [m/s]

K=L/(v+c);
x = 0.5 * (1 - 1/ (L * 1/a* T* Sb / q) );

if x > 0.5
    x = 0.5;
end

if x<0
    x = 0;
end