%Testing the steady state profile

clear 
close all
addpath('../../Final')

%input variables
SimTime=13500; %in seconds
s=0.0015;
L=7000;
Bw=3;
z=0;
n=0.03;
dt=10;
SimLength=floor(SimTime/dt)+1;
qu=ones(1,SimLength+1)*1.5;
qd=ones(1,SimLength+1)*1.5;

qu(2:floor(3600/dt))=3;
q=1.5;
h=3;
NodeNum=1000;
HinitGiven=0;
UseInitialization=0;
% 

% 
 %Sobek 
 [ Time,Hm2 , Hm,x] = FiniteVoumeModel11bck(L, z,Bw,n,s,SimTime,dt,qu,qd,h,NodeNum, UseInitialization, HinitGiven );
 %%
 close all
 

 sobek_down=csvread('WaterDepthDownstream4.csv',1,0);
 sobek_up=csvread('WaterDepthUpstream4.csv',1,0);

  
 st_sobek=10;

%Plotting
%figure('rend','painters','pos',[10 10 1300 600])
figure
MyFont=14;
factor=3600;

plot((0:st_sobek:(length(sobek_down)-1)*st_sobek)/factor, sobek_down)
 hold on
 plot(Time/factor,Hm2(:,end),'--r')
xlim([0 3.5])

legend({'Sobek','Saint-Venant solver'},'Location','best', 'FontSize',MyFont)
xlabel('Time (h)', 'FontSize',MyFont)
ylabel('Water depth (m)', 'FontSize',MyFont)
title('Downstream water level', 'FontSize',MyFont)




figure
MyFont=14;


plot((0:st_sobek:(length(sobek_up)-1)*st_sobek)/factor, sobek_up)
 hold on
 plot(Time/factor,Hm2(:,2),'--r')


legend({'Sobek','Saint-Venant solver'},'Location','best', 'FontSize',MyFont)
xlabel('Time (h)', 'FontSize',MyFont)
ylabel('Water depth (m)', 'FontSize',MyFont)
title('Upstream water level', 'FontSize',MyFont)
xlim([0 3.5])


