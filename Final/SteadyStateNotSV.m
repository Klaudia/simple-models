function [ HinitH, distance] = SteadyStateNotSV(L, z,Bw,n,s,q,h,NodeNum, option_chezy, option_proffactor)
%SteadyState Profile
%Klaudia Horv�th, Delft, 2016.12.21.
%Modified: 2017.01.15.
switch nargin
    case 8
        option_chezy=1;
        option_proffactor=1;
    case 9    
        option_proffactor=1;
end

% First step: known variables
g=9.81;
T=Bw+z*h*2;
a=(Bw+T)*h/2;
P=Bw+((T-Bw)^2/4+h^2)^0.5*2;
ch=1/n*(a/P)^(1/6);
n=1/ch/h^(1/6);


%% Initial profile
ProfFactor=option_proffactor;
%ProfFactor=1;
QNum=NodeNum-1;
%dxf=ones(1,QNum)*L/QNum;
dx=L/(NodeNum-1);
%NodeNumProfile=(NodeNum*2-1-1)+1;
dxProfile=dx/ProfFactor;
%hn = fminbnd(@(h) NormalFlow(z,Bw,n,s,q,h),0,10);

for i=1:(NodeNum-1)*ProfFactor+1
     T=Bw+z*h*2;
     a=(Bw+T)*h/2;
     P=Bw+((T-Bw)^2/4+h^2)^0.5*2;
     R=a/P;
     fr=((q^2*T)/(g*a^3))^0.5;
     chm(i)=1/n*R^(1/6);
     if option_chezy==1
     Sf=q^2*(1/ch)^2/(a^2*R^(1));
     else
     Sf=q^2*(n)^2/(a^2*R^(4/3));
     end
     dydx=(s-Sf)/(1-fr^2);
     hnew=h-dxProfile*dydx;
     hmat(i)=h;
     h=hnew;
     distance_back(i)=L-(i-1)*dxProfile;
     distance_(i)=(i-1)*dxProfile;
end
HinitH=hmat(end:-ProfFactor:1);
distance=distance_(1:ProfFactor:end);
%HinitH=hmat;

