%Flow categories
%Klaudia Horv�th, 2016.01.10. Delft
clear all
close all
addpath('..\Final')
addpath('..\colorbarfunctions')
%Note: it is using function related to the colorbar, but they work only for
%      old matlabs. So for this version it is commented.
ToSave=0;
%load ChannelData

roundfactor=100;                        %?????????????
g=9.81;                                 %Constante g=9.81
Discretizedq=50;                        %Gediscretiseerde stappen q en h in 30 stappen
Discretizedh=50;

%% Situations Rectangular channels

%DataRectangularChannels=ones(2,11);                  %(number of cases, number of variables)
%Define Situation 1 by filling in the data matrix Defining Types (1,3,4,5)
%for rectangular channels
DataRectangularChannels(1,4)=7000;                  %Length
DataRectangularChannels(1,3)=0;                      %Side Slope for rectangular channels 0
DataRectangularChannels(1,1)=3;                      %Bottom Width
DataRectangularChannels(1,5)=0.02;                   %Manning Coefficient
DataRectangularChannels(1,2)=3e-4;                   %Bottom Slope          chi= bottom slope*length/reference depth * (5/3)
DataRectangularChannels(1,8)=1.9;                      %Water Level
DataRectangularChannels(1,7)=4;                      %Minimun Water Level
DataRectangularChannels(1,6)=6;                     %Maximum Water Level
DataRectangularChannels(1,11)=2;                   %Discharge (Average)
DataRectangularChannels(1,9)=1.9;                      %Minimun Discharge     
DataRectangularChannels(1,10)=3;                    %Maximum Discharge

       
%Situation 2 Defining Types (1,3,4,5) for Trapezoidal channels
% DataRectangularChannels(2,4)=60000;                  %Length
% DataRectangularChannels(2,3)=0.5;                      %Side Slope for rectangular channels 0
% DataRectangularChannels(2,1)=5;                      %Bottom Width
% DataRectangularChannels(2,5)=0.02;                   %Manning Coefficient
% DataRectangularChannels(2,2)=1e-4;                   %Bottom Slope          chi= bottom slope*length/reference depth * (5/3)
% DataRectangularChannels(2,8)=4.6;                    %Water Level
% DataRectangularChannels(2,7)=4;                      %Minimun Water Level
% DataRectangularChannels(2,6)=10;                    %Maximum Water Level
% DataRectangularChannels(2,11)=6;                     %Discharge (Average)
% DataRectangularChannels(2,9)=0;                      %Minimun Discharge     
% DataRectangularChannels(2,10)=15;                    %Maximum Discharge

% %Finding type 2
% DataRectangularChannels(3,4)=5000;                  %Length
% DataRectangularChannels(3,3)=0;                      %Side Slope for rectangular channels 0
% DataRectangularChannels(3,1)=5;                      %Bottom Width
% DataRectangularChannels(3,5)=0.02;                   %Manning Coefficient
% DataRectangularChannels(3,2)=5e-4;                   %Bottom Slope          chi= bottom slope*length/reference depth * (5/3)
% DataRectangularChannels(3,8)=4.6;                    %Water Level
% DataRectangularChannels(3,7)=2;                      %Minimun Water Level
% DataRectangularChannels(3,6)=5.5;                    %Maximum Water Level
% DataRectangularChannels(3,11)=6;                     %Discharge (Average)
% DataRectangularChannels(3,9)=0;                      %Minimun Discharge     
% DataRectangularChannels(3,10)=10;                    %Maximum Discharge

%Define Variables using the data matrix
lm=DataRectangularChannels(:,4);                     %Length
zm=DataRectangularChannels(:,3);                     %Side Slope
bm=DataRectangularChannels(:,1);                     %Bottom Width
nm=DataRectangularChannels(:,5);                     %Manning Coefficient
sm=DataRectangularChannels(:,2);                     %Bottom Slope
hm=DataRectangularChannels(:,8);                     %Water Level
hminm=DataRectangularChannels(:,7);                  %Minimun Water Level
hmaxm=DataRectangularChannels(:,6);                  %Maximum Water Level
qm=DataRectangularChannels(:,11);                    %Discharge (Average)
qminm=DataRectangularChannels(:,9);                  %Minimun Discharge     
qmaxm=DataRectangularChannels(:,10);                 %Maximum Discharge


%%
for k=1:length(lm)                  %Define the number of situations
    hmin=hminm(k);                  %Per situaties de h_min, h_max, q_min en q_max uitgesplits in de loop
    hmax=hmaxm(k);
    qmin=qminm(k);
    qmax=qmaxm(k);
    
    qvar=linspace(qminm(k),qmaxm(k),Discretizedq);          %q per situatie linear verdeeld tussen q_min en q_max
    hvar=linspace(hminm(k),hmaxm(k),Discretizedh);          %h per situatie linear verdeeld tussen h_min en h_max
    for qt=1:length(qvar)                                   %30 stappen (Hoeveelheid stappen afhankelijk van gediscretiseerde stappen in q) 
        for ht=1:length(hvar)                               %30 stappen (Hoeveelheid stappen afhankelijk van gediscretiseerde stappen in h)
                                            %Example beneden Linge, wave dameped, first order
            z=zm(k);                        %one step up how much widens
            b=bm(k);
            n=nm(k);
            s=sm(k);
            l=lm(k);
            q=qvar(qt);
                      
            SimTime=86400/24*12;            %in seconds (12 uur)
            dt=15;                          %timestep

            
            %I'm still not sure why this loop is in here?
            if qt==1                                %set initial values for the first step of the discretization
                if ht==1
                        hinit=hm(k);                %initial waterlevel
                     if k==1                        %first situation different timesteps??? is the necessary because only the dt is different?????
                         dt=1000;
                         SimTime=86400/24*3;
                     else
                         dt=15;
                         
                     end
                    SimLength=floor(SimTime/dt)+1;      %amounth of timesteps    
                    qu=ones(1,SimLength+1)*qm(k);       %make collumn of q depending on the amounth of timesteps
                    qu(2:floor(3600/dt))=qvar(end);     %qu means q upstream 
                    qd=ones(1,SimLength+1)*qm(k);       %qd is different from qu because above the first 240 values are editted what is the reason for this????
                    %plot(qu) to see the upstream discharge
                    %plot(qd) to see the downstream discharge
%[Time,Hm, Hm3, HinitH]=FiniteVoumeModel9(l,z,b,n,s,30000,dt,qu,qd,hinit );                    
                    [ Time,Hm2 ] = FiniteVoumeModel11bck(l,z,b,n,s,SimTime,dt,qu,qd,hinit,50, 0, 0);        %Hm starting with average heigth?? over time?
                    %Plotting plot(Time,Hm2(:,65(last collumn)) gives Step
                    %response at the downstream end, collumns before the 
                    %last are at a diffent place in the channel. first 
                    %collum is at the upstream end. last collumn is at the 
                    %downstream end.
               
                   if k==1
                       Time1=Time;
                                           Hsim1(1:SimLength)=Hm2(:,end);
                   else
                                           Hsim(k,1:SimLength)=Hm2(:,end);        %saving Hm2 for plotting for every situation in rows
                                           %plot(Time,Hsim(k,:)) with k
                                           %for every situation
                                           %Step Response (Time(h) vs Water
                                           %depth(m))
                   end
                end
            end
           
             qd(1)=qm(k);
             qd(1)=q;                       %First value of discharge

             hinitpr=hvar(ht);              %gediscretiseerde stappen H_av step ht=30???????
            [ Timex,Hm2x , Hmx,HinitH] = FiniteVoumeModel11bck(l, z,b,n,s,1,dt,qu,qd,hinitpr,50,0,0 );
            
            %Normal depth, not used
            hn1 = NormalDepth(q,n,b,z,s,hinitpr);

            NormLoc=0;
            
            %Determining where the point is where normal flow ends.
            
             for wnum=length(HinitH):-1:1
                 if HinitH(wnum)<hn1+0.01
                   NormLoc=wnum;     
                 end
             end
             
            %hn=HinitH(floor(length(HinitH)/2));
            hn= HinitH(floor(NormLoc+(length(HinitH)-NormLoc)/2));
            %The water depth used
            hn2=hvar(ht);
            
            %Calculation of the parameters
            t=b+z*hn*2;                                             %calculation T = B+2zy
            a=(b+t)*hn/2;                                           %
            v=q/a;                                                  %    
            fr=((q^2*t)/(g*a^3))^0.5;                               %Froude number %Cannot be higher than 1
            hc=v^2/g;                                               %
            p=b+2*((t-b)^2/4+hn^2)^0.5;                             %        
            r=a/p;                                                  %    
            ch=1/n*r^(1/6); %Chezy                                  %    
            sf=q^2*n^2/(a^2*r^(4/3));                               %Friction Slope *Manning equation    
            dydx=(s-sf)/(1-fr^2);                                   % 

            
            
            chi0=s*l/hn; %Original                   equations 1 and 2 for normal flow
            eta0=chi0/(fr*(1-fr)); %Original
            
            %for rectangular channels (equation (4)
            %Sb*Xr*(g/(v*(sqrt(g*yr)-v))
            
            clit=5*q/(3*b*hn);  %Litrico parameter   Rectangular channels
            dlit=q/(2*b*sf);    %Litrico parameter   Rectangular channels
            %Above formulas for normal flow
            
            
            
            
            dme=q/(2*t*sf);     %Litrico parameter not only for normal flow
            y=hn;

            p1=sf*(-10/3*(b+2*z*hn)+8/3*r*(1+z^2)^0.5)/a;
            p2=2*z;
            p3=1; %But finally we approximate dy/dz as 1

            %cbrot=(q/(2*b^2*sf))*10*b*sf/3/a*b; %C according to Brotons
            cme=q/(2*t^2*sf)*(2*z*dydx-2*z*sf-t*(p1)); %This is the C from Litrico's paper, calculate by hand

            chi=l/2*cme/dme*3/5;  %This is the chi, coming from Litrico
            damping=2*g*q/(ch^2*r*a)/(2*((8*g*hn/l^2)^(1/2)));
            eta=damping*3;        %This is the eta used, based on PJ
            %eta2=chi/(fr*(1-fr)); %This is the Baume type eta, based on my chi
            
            if eta<3
                if chi<0.6
                    type=1;
                elseif chi<1.35
                    type=2;
                else
                    type=0;
                end
            else
                
                if chi<0.6
                    type=3;
                elseif chi<1.35
                    type=4;
                else
                    type=5;
                end
            end
            
            M(k,1)=b;
            M(k,2)=s;
            M(k,3)=z;
            M(k,4)=l;
            M(k,5)=q;
            M(k,6)=round(hn*roundfactor)/roundfactor;
            M(k,7)=round(chi*roundfactor)/roundfactor;
            M(k,8)=round(eta*roundfactor)/roundfactor;
            M(k,9)=type;
          
            etam(k,qt,ht)=eta;
            %eta2m(k,qt,ht)=eta2;
            chim(k,qt,ht)=chi;
            chi2m(k,qt,ht)=chi0;
            typem(k,qt,ht)=type;
            hnn(k,qt,ht)=hn1;
            den(qt,ht)=fr*(1-fr);
            test(qt,ht)=hn;
            test2(k,qt,ht)=HinitH(1);
            test3(qt,ht)=HinitH(end);
            hcm(k,qt,ht)=hc;
            %eta2m(k,qt,ht)=eta2;
            clitm(k,qt,ht)=clit;
            cmem(k,qt,ht)=cme;
            dlitm(k,qt,ht)=dlit;
            dmem(k,qt,ht)=dme;
         end
    end
end

%% Plotting

for k=1:length(lm)
%for    k=3:3
    qvar=linspace(qminm(k),qmaxm(k),Discretizedq);
    hvar=linspace(hminm(k),hmaxm(k),Discretizedh);
        
    f1=figure(k);
   
    subplot(2,2,1)
%    surf(hvar,qvar,squeeze(etam(k,:,:)));
    h=pcolor(hvar,qvar,squeeze(etam(k,:,:)));
    set(h, 'EdgeColor', 'none');

    [row col]=size(squeeze(etam(k,:,:)));
    etaMatr=squeeze(etam(k,:,:));
    etaMatr2=squeeze(etam(k,:,:));

    for rowc=1:row
        for colc=1:col
            if etaMatr(rowc,colc)>3
                etaMatr2(rowc,colc)=0;
            else
                etaMatr2(rowc,colc)=1;
            end
        end
    end
    hold on
    [C,h] =contour(hvar,qvar,etaMatr2,1);
    set(h,'LineColor','w')

    ylabel('Discharge (m^3/s)')
    xlabel('Water depth (m)')
    view([0 90])
    
    title(['\eta value for test case ' num2str(k)])
    axis tight
    colormap(hot)
    h=colorbar;
 %   cbfreeze
  %  freezeColors
    
    subplot(2,2,2)
    
    
    [row col]=size(squeeze(chim(k,:,:)));
    chiMatr=squeeze(chim(k,:,:));
    chiMatr2=squeeze(chim(k,:,:));

    for rowc=1:row
        for colc=1:col
            if chiMatr(rowc,colc)>1.35
                chiMatr2(rowc,colc)=0;
            elseif    chiMatr(rowc,colc)>0.6
                chiMatr2(rowc,colc)=1;
            else
                chiMatr2(rowc,colc)=2;
            end
        end
    end
 

    %surf(hvar,qvar,squeeze(chim(k,:,:)))
    h=pcolor(hvar,qvar,squeeze(chim(k,:,:)));
    set(h, 'EdgeColor', 'none');
    hold on

    [C3,h3] =contour(hvar,qvar,chiMatr2,2);
    LevListVector=    unique(squeeze(chim(k,:,:)));

    %set(h3,'LevelList',LevListVector)
    set(h3,'LineColor','k')
    ylabel('Discharge (m^3/s)')
    xlabel('Water depth (m)')
    view([0 90])
    colorbar
    colormap(cool)
%    cbfreeze
 %   freezeColors
    title(['\chi value for test channel ' num2str(k)])
    axis tight
    
    
    subplot(2,2,3)
    

    h=pcolor(hvar,qvar,squeeze(typem(k,:,:)));
    set(h, 'EdgeColor', 'none');
    ylabel('Discharge (m^3/s)')
    xlabel('Water depth (m)')
    view([0 90])
    
    title('Type for test channel 2')
    
    
    
    colormap(jet(5))
    hcb=colorbar;
    hold on
    contour(hvar,qvar,squeeze(typem(k,:,:)),1)

    %colorbar
    set(gca, 'clim', [0.5 5.5]);
    set(hcb,'YTick',[1,2,3,4,5])
    axis tight
    SaveName=['Case' num2str(k) ];

    subplot(2,2,4)

    factor=3600;
    if k==1
       plot(Time1/factor,Hsim1(:))
    else
       plot(Time/factor,Hsim(k,:))

    end

    xlabel('Time (h)')
    ylabel('Water depth (m)')
    title('Step response')
   axis tight
   if k==1
       xlim([0 max(Time1/factor)])
       Buffer=(-min(Hsim1(:))+max(Hsim1(:)))/10;
       ylim([min(Hsim1(:)) max(Hsim1(:))+Buffer])   
   else
       xlim([0 max(Time/factor)])
       Buffer=(-min(Hsim(k,:))+max(Hsim(k,:)))/10;
       ylim([min(Hsim(k,:)) max(Hsim(k,:))+Buffer])
   end
   
   if ToSave==1
        saveas(f1,SaveName,'fig')
        saveas(f1,SaveName,'epsc')
   print(f1,'-dpdf',[SaveName '.pdf'],'-r500')
      print(f1,'-dpng',[SaveName '.png'],'-r500')

   end
   
    
end
%

