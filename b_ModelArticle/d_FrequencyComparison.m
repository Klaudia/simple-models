%Model comparison
%Klaudia Horv�th, Delft, 2016
%Modified: 2017.04.07.

%Later to do:
%Implement in Hayami if something comes from downstream
%Implemetn the dossteream normally for ID and for the ohter stuff

%Id
%IDZ
%hayami
%SV

%IR
%In


%This is the same like 4, but without Sobek
clear all
close all
addpath('..\Final')
addpath('../Final/LinearModels')


%The physical data of the reach
ToSave=1;
CaseNumber=1;
StepNumber=2;
ModelType=2; % ModelType=2; %LinearizedInertial % ModelType=1; %Lineareized SV % ModelType=3; %Linear model 

dt=10; %For case 1 use 0.5
EndTime=5*3600;
NodeNumGiven=30;
%NodeNumGiven=50;

%for case 5 node numer is
%NodeNumGiven=18;

%Case 1
% dt=0.25; %For case 1 use 0.5
% EndTime=2*3600;
% NodeNumGiven=8;


%% Data channels (zelfde als bij het andere script)
%load ChannelData

lm=7000; %It works well for 700
zm=2;
bm=3;
nm=0.02;
sm=1.5e-4;
wm=1.5;
hminm=0.5;
hmaxm=1.5;
qqm=2.5;
qminm=2;
qmaxm=3;


% lm=7000;
% zm=0;
% bm=3;
% nm=0.02;
% sm=3e-4;
% wm=1;
% hminm=0.5;
% hmaxm=1.6;
% qqm=1.5;
% qminm=2;
% qmaxm=3;

%Nonlinearity parameter
%how widht change of water depth changes the bw area
%How the water dpeth changes with the voulume --> Backwater area is small,
%even when we have backwater, so L*B is small compared to the volume
%Or the backwater area changes a lot, that occurs when 
%Delta q / (LB) here 0.5/
%Say if a normal discharge thing can cause more than 80 cm, then wrong.
%dq/(LB)<0.8

% pick initial values for 1 case
n=nm(CaseNumber);
Sb=sm(CaseNumber);
L=lm(CaseNumber);
B=bm(CaseNumber);
m=zm(CaseNumber);
q=qqm(CaseNumber); %This is the discharge around which we linearize
Y0=wm(CaseNumber); %This is the h around which we liearize
qmax=qmaxm(CaseNumber);

DischargeStepMatrix=[q qqm qmax];
%DischargeStep=DischargeStepMatrix(StepNumber);
DischargeStep=qmax-qqm;
NominalDischarge=qqm;
kh=0;
StepFinish=floor(3600/dt);

Distance=L/(NodeNumGiven-2);
NumberHPoints=(NodeNumGiven-2)/2;
XMatrix=[];   
sxx=(Sb-sf0(Y0,q,n,B,m))/(1-(froude(Y0,q,n,B,m))^2);

%Initialize variables
x2M=zeros(NumberHPoints,1);
y2M=zeros(NumberHPoints,1);

 p21_infM=zeros(NumberHPoints,1);
%  tdM(k)=zeros(NumberHPoints,1);
%  tauIDupM(k)=zeros(NumberHPoints,1);
tdMD=zeros(NumberHPoints,1);
tauIDupMD=zeros(NumberHPoints,1);
IDDownAs=zeros(NumberHPoints,1);
IDUpAs=zeros(NumberHPoints,1);
HayR=zeros(NumberHPoints,1);
HayParameters=zeros(NumberHPoints,5);
aa=zeros(NumberHPoints,1);
bb=zeros(NumberHPoints,1);
cc=zeros(NumberHPoints,1);
dd=zeros(NumberHPoints,1);
ee=zeros(NumberHPoints,1);
tauhay=zeros(NumberHPoints,1);
p12_infM=zeros(NumberHPoints,1);
tuMD=zeros(NumberHPoints,1);
tauIDdownMD=zeros(NumberHPoints,1);
auM=zeros(NumberHPoints,1);


%First model
[~, ~, ~, ~, au, ad, ~, ~, yn,xmid]=IdzFun(q,n,B,m,Sb,Y0,L);

[AdownID, tau]=IdFunBw(q,n,B,m,Sb,Y0,L,kh); 

 

[p11_inf, p12_inf, p21_inf, p22_inf, au, ad, tu, td, yn, yu]=IdzFun(q,n,B,m,Sb,Y0,L);
sysidz=tf([p21_inf*ad 1],[ad 0],'InputDelay',td);



%Calculating the parameters of the ID model

[a, tau]=IdFunBw(q,n,B,m,Sb,Y0,L,kh);
sysid=tf([1],[AdownID 0],'InputDelay',tau);

%The linearized inertial model
[x, A, Bd, D,Hinit]=LinearInertial(m,B,Y0,n,q,Sb,L,dt,NodeNumGiven,ModelType);
C=zeros(1,length(A));
C(end)=1;
dt=10;
sysinertial=ss(A,Bd,C,0,dt);

%Hayami
[aa, bb cc dd ee tauhay, cl, yn ccnoni ddnoni eenoni G K1 tau ae]=FunHayami(q,n,B,m,Sb,Y0,L,kh,dt);
 if cl<1
     syshay=tf([G/ae],[K1 1 0]);
 elseif cl<9/4
     syshay=tf([G/ae],[K1 1 0],'InputDelay',tau);
 else
     syshay=tf([G/ae],[K1(1)*K1(2) K1(1)+K1(2) 1 0],'InputDelay',tau);
 end
 
  [num,den,sysidr,Mr_calc,Om_r_calc, num2]=ident_idr_new(B ,Y0 , m ,q ,L,n,0,0);
[k1 p21_mag p21_phase_corr p22_mag p22_phase_corr ]=Bode_fun(m, B, Sb, n,q, L, Y0, 0, 0, 0, 0);
 
 
syslist={sysid sysidz sysidr syshay sysinertial} ;
discretization_numbers=500;
magnitude_models=zeros(discretization_numbers,length(syslist));
phase_models=zeros(discretization_numbers,length(syslist));

for k=1:length(syslist)
[mag,pha,w_d] = bode(syslist{k},logspace(-4,-1,discretization_numbers));
magnitude_models(:,k)=squeeze(mag(1,1,:));
phase_models(:,k)=squeeze(pha(1,1,:));
end
%%
figure

for k=1:length(syslist)
semilogx(w_d,20*log10(magnitude_models(:,k)))
hold on

end
semilogx(k1,p21_mag,'k')
legend('sysid', 'sysidz', 'sysidr', 'syshay', 'sysinertial', 'sv')
grid

% [mag_d,phase_d,w_d] = bode(sys22,logspace(-5,-2.3,5000));
% mag2_d=ones(1,55);
% phase2_d=ones(1,55);
% for i=1:length(mag)
%     
% mag2_d(i)=mag_d(1,1,i);
% phase2_d(i)=phase_d(1,1,i);
% 
% end
% 
% 
% print(h2,'-dpng','BodeIrp21_com')
% saveas(h2,'BodeIrp21_com.fig')
% 
% h3=figure(3);
% 
% bode(sys22)
% title('Bode plot of the IR model, p21')
% print(h3,'-dpng','BodeIrp22')
% saveas(h3,'BodeIrp22.fig')
% 
% 
% h4=figure(4);
% subplot(2,1,1)
% 
% semilogx(w_d,20*log10(mag2_d))
% hold on
% semilogx(k1,ga_22,'r')
% 
% title('Phase plot of the transfer function p22 of CF and the IR model')
% grid
% xlabel('Frequecncy (rad/s)')
% ylabel('Magnitide (dB)')
% axis([0.00001 0.002 -100 -20])
% 
% subplot(2,1,2)
% 
% semilogx(k1,ga_phase_22+180,'r')
% hold on
% semilogx(w,phase2_d)
% 
% xlabel('freq. (rad/s)')
% ylabel('phase (dg)')
% grid
% axis([0.00001 0.002 0 400])
% 
% 
% print(h4,'-dpng','BodeIrp22_com')
% saveas(h4,'BodeIrp22_com.fig')
