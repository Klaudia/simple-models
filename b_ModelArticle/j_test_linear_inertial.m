%TestLinearInertial
%% Data channels (zelfde als bij het andere script)
%load ChannelData
clear 
close all
addpath('../Final/LinearModels')

%The physical data of the reach
CaseNumber=1;
ModelType=2; % ModelType=2; %LinearizedInertial % ModelType=1; %Lineareized SV % ModelType=3; %Linear model 

dt=10; %For case 1 use 0.5
EndTime=15*3600;
NodeNumGiven=30;


n=0.02;
Sb=1.5e-4; %Bottom slope
L=7000;
B=3;
m=2; %Side slope
Y0=1.5; %This is the h around which we liearize
qmax=3;
q_nominal = 2.5;%This is the discharge around which we linearize

DischargeStep=qmax-q_nominal;
NominalDischarge=q_nominal;

StepFinish=floor(3600/dt);
SimLength=floor(EndTime/dt)+1;
InitializationTime = 3600;


[x, A, Bd, D,Hinit]=LinearInertialImp(m,B,Y0,n,q_nominal,Sb,L,dt,NodeNumGiven,ModelType);
system_size = size(A);
c = zeros (1, system_size(1));
c (end) = 1;

q=ones(SimLength+InitializationTime+2,1)*0;
q(1:InitializationTime+StepFinish,1)=DischargeStep;
q(1:InitializationTime)=0;
%%
for k=1:SimLength 
    xold=x;
    x=A*x+Bd*D;
    y (k)= c*x;
    qb=q(k);
    hb=0;
    D=[qb;hb];   
    hm(k,:)=xold(1:2:end)'; %Store water level
    qm(k,:)=xold(2:2:end)'; %Store discharge
end
plot(hm(:,end))