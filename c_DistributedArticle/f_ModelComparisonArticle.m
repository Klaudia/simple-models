%Model comparison
%Klaudia Horv�th, Delft, 2016
%Modified: 2017.04.07.

%this file compares step responses
%It does not load anything
%It uses functions like:
% -
% -
% -

%Later to do:
%Implement in Hayami if something comes from downstream
%Implemetn the dossteream normally for ID and for the ohter stuff

%This is the same like 4, but without Sobek
clear 
close all
tic
addpath('..\Final')
addpath('../Final/LinearModels')
addpath('../DistributedModels')


%The physical data of the reach
ToSave=1;
CaseNumber=1;
StepNumber=2;
ModelType=2; % ModelType=2; %LinearizedInertial % ModelType=1; %Lineareized SV % ModelType=3; %Linear model 

dt=10; %For case 1 use 0.5
EndTime=20*3600;
NodeNumGiven=30;
%NodeNumGiven=50;
g=9.81; 
%for case 5 node numer is
%NodeNumGiven=18;

%Case 1
% dt=0.25; %For case 1 use 0.5
% EndTime=2*3600;
% NodeNumGiven=8;


% Data channels (zelfde als bij het andere script)
%load ChannelData

% lm=7000;
% zm=2;
% bm=3;
% nm=0.02;
% sm=1.5e-4;
% wm=1.5;
% hminm=0.5;
% hmaxm=1.5;
% qqm=2.5;
% qminm=2;
% qmaxm=3;

sm=0.0015;
lm=3000;
bm=3;
zm=0;
nm=0.03;
wm=3;
qqm=1.5;
qmaxm=3;

sm=0.0015;
wm=1;


% lm=7000;
% zm=0;
% bm=3;
% nm=0.02;
% sm=3e-4;
% wm=1;
% hminm=0.5;
% hmaxm=1.6;
% qqm=1.5;
% qminm=2;
% qmaxm=3;

%Nonlinearity parameter
%how widht change of water depth changes the bw area
%How the water dpeth changes with the voulume --> Backwater area is small,
%even when we have backwater, so L*B is small compared to the volume
%Or the backwater area changes a lot, that occurs when 
%Delta q / (LB) here 0.5/
%Say if a normal discharge thing can cause more than 80 cm, then wrong.
%dq/(LB)<0.8

% pick initial values for 1 case
n=nm(CaseNumber);
Sb=sm(CaseNumber);
L=lm(CaseNumber);
B=bm(CaseNumber);
m=zm(CaseNumber);
q=qqm(CaseNumber); %This is the discharge around which we linearize
Y0=wm(CaseNumber); %This is the h around which we liearize
qmax=qmaxm(CaseNumber);
q_steady=q;
[ HinitH, distance] = SteadyState(L, m,B,n,Sb,q,Y0,50);
%%
DischargeStepMatrix=[q qqm qmax];
%DischargeStep=DischargeStepMatrix(StepNumber);
DischargeStep=qmax-qqm;
NominalDischarge=qqm;
kh=0;
StepFinish=floor(3600/dt);

Distance=L/(NodeNumGiven-2);
NumberHPoints=(NodeNumGiven-2)/2;
XMatrix=[];   
sxx=(Sb-sf0(Y0,q,n,B,m))/(1-(froude(Y0,q,n,B,m))^2);

%Initialize variables
x2M=zeros(NumberHPoints,1);
y2M=zeros(NumberHPoints,1);

 p21_infM=zeros(NumberHPoints,1);
%  tdM(k)=zeros(NumberHPoints,1);
%  tauIDupM(k)=zeros(NumberHPoints,1);
tdMD=zeros(NumberHPoints,1);
tauIDupMD=zeros(NumberHPoints,1);
IDDownAs=zeros(NumberHPoints,1);
IDUpAs=zeros(NumberHPoints,1);
HayR=zeros(NumberHPoints,1);
HayParameters=zeros(NumberHPoints,5);
aa=zeros(NumberHPoints,1);
bb=zeros(NumberHPoints,1);
cc=zeros(NumberHPoints,1);
dd=zeros(NumberHPoints,1);
ee=zeros(NumberHPoints,1);
tauhay=zeros(NumberHPoints,1);
p12_infM=zeros(NumberHPoints,1);
tuMD=zeros(NumberHPoints,1);
tauIDdownMD=zeros(NumberHPoints,1);
auM=zeros(NumberHPoints,1);


%First model
[~, ~, ~, ~, au, ad, ~, ~, yn,xmid]=IdzFun(q,n,B,m,Sb,Y0,L);
[p11_info, p12_info , p21_info, p22_info, auo, ado, ~, ~, yn,xmid]=IdzFun(q,n,B,m,Sb,Y0,L);

xsep=2*xmid-L;
[AdownID, tau]=IdFunBw(q,n,B,m,Sb,Y0,L,kh); 
AupID=1e9; %Upstream nothing happens

sv=0;
[tuM_id,tdM_id,auM_id,adM_id]=IdFunDis2(q,n,B,m,Sb,Y0,L, NodeNumGiven,sv);
toc
%% This loop caclulates the parameters for all water level points in a reach
for k=1:NumberHPoints+1
    x2=Distance+Distance*2*(k-1)-Distance; %x2 is the horizontal distance from upstream of the chosen point
    x2m(k)=x2;
    y2=Y0-(L-x2)*sxx; %y2 is the water level belonging to x2
    if y2<yn
        y2=yn;
    end
    %Stores in matrix the longitudinal distance and the corresponding water
    %level
    x2M(k)=x2;
    y2M(k)=y2; %good
    %This is the transfer function from upstream to a certain point
    %That is why we are taking the corresponding water depth (that we
    %calculate) and the length that is from upstream to that point (x2)
    %For p21 inf and taud Up, the upstream part
    [p11_inf, p12_inf , p21_inf, p22_inf, au, ad, tu, td , ~]=IdzFun(q,n,B,m,Sb,y2,x2);
  %  [p11_inf_hat p12_inf_hat p21_inf_hat p22_inf_hat au_hat ad_hat tu_hat td_hat yn x2]=
    [~, tau]=IdFunBw(q,n,B,m,Sb,Y0,x2,kh); 
    %[aa_ bb_ cc_ dd_ ee_ r cl yn cnoni dnoni enoni]=FunHayami(q,n,B,m,Sb,y2,x2,kh,dt);
    %tauhay(k)=r;
    %HayParameters(k,:)=[aa bb cc dd ee];
    if x2<xsep
           IDUpAs(k)=AupID;
    else
           IDUpAs(k)=AdownID;
    end
%     aa(k)=aa_;
%     bb(k)=bb_;
%     cc(k)=cnoni/IDUpAs(k);
%     dd(k)=dnoni/IDUpAs(k);
%     ee(k)=enoni/IDUpAs(k);
    p21_infM(k)=p21_inf; %transfer function between ups q and down h
    tdMD(k)=floor(td/dt); %the discretized parameters
    tdM(k)=td; %the discretized parameters

    tauIDupMD(k)=floor(tau/dt); %the discretized parameters
    %----------------------------------------------------------------------
    %These are the effects from downstream to all water levels
    [p11_inf_, p12_inf_ , p21_inf_, p22_inf_, au_, ad_, tu_ , td_, ~]=IdzFun(q,n,B,m,Sb,Y0,L-x2);  %Think about this claim for later for the distributed
    testy(k)=Y0;
    testx2(k)=x2;
    [~, tau]=IdFunBw(q,n,B,m,Sb,Y0,L-x2,kh);
    IDDownAs(k)=AdownID; %This should be an if statement such as above
    p12_infM(k)=p12_inf_;
    tuM(k)=tu_;

    tuMD(k)=floor(tu_/dt);
    tauIDdownMD(k)=floor(tau/dt);
     %The backwater area is calculated proportinally from the originals
     %Write these findings down
     %So... if x2 is up, use au, if lower, use ad
%    auM(k)=x2/L*au+(L-x2)/L*ad;
     if x2<xsep
         auM(k)=au;
     else
         auM(k)=ad; 
     end
     ad_matrix(k)=ad_*(1+(ad/au_));
     au_matrix(k)=au*(1+(au_/ad));
     %the inverse sum
     au_matrix_test(k)=ad;
     au_matrix_test1(k)=au_;
     p11m(k)=p11_inf;
     p12m(k)=p12_inf;
     p21m(k)=p21_inf;
     p22m(k)=p22_inf;
     
     p11m_(k)=p11_inf_;
     p12m_(k)=p12_inf_;
     p21m_(k)=p21_inf_;
     p22m_(k)=p22_inf_;
     
     pum(k)=p21_inf-(p21_inf*p22_inf)/(p11_inf_+p22_inf);
     pdm(k)=-(p12_inf_*p22_inf)/(p11_inf_+p22_inf);
     puma(k)=ad+au_;
end
pdm(1)=p12_info;
pdm(end)=-p22_info;

pum(1)=p11_info;
pum(end)=p12_info;


[tuM_,tdM_,pum_,pdm_,puma_,x]=IdzFunDist(q,n,B,m,Sb,Y0,L, NodeNumGiven);
tuMD_=floor(tuM_./dt);
tdMD_=floor(tdM_./dt);

%%
SpacePoints=NodeNumGiven*10;
Startexp=-3.5;
Finexp=-3;
TimePoints=0;
[freq, p21_mag_dist, p22_mag_dist, p11_mag_dist, p12_mag_dist, appra11, appra12, appra21, appra22 ]...
    =BackwaterAreaApproximation(m, B, Sb, n,q_steady, L, Y0, Startexp, Finexp, TimePoints,SpacePoints);
SimTime=3600*5;
dt=10;
qu=ones(SimTime+100,1)*q;
qd=qu;
qu(1:360)=q*2;
UseInitialization=0;
HinitGiven=0;
NodeNum=50;
% [ Time,Hm2 , Hm,HinitH] = FiniteVoumeModel11bck...
%     (L, m,B,n,Sb,SimTime,dt,qu,qd,Y0,NodeNum, UseInitialization, HinitGiven );

% figure
% plot(Time,Hm2(:,end))
% hold on

%%
%Finexp=log10((g*Y0)/(2*x))+2;
Startexp=0;
Finexp=2;
TimePoints=100;
SpacePoints=50;
 [freq, p21_mag_dist, p22_mag_dist, p11_mag_dist, p12_mag_dist, appra11,...
     appra12, appra21, appra22 ]=BackwaterAreaApproximation...
     (m, B, Sb, n,q, L, Y0, Startexp, Finexp, TimePoints,SpacePoints);

[p11_inf, p12_inf, p21_inf, p22_inf, au, ad, tu, td, yn, yu]=IdzFun(q,n,B,m,Sb,Y0,L);
%Calculating the parameters of the ID model

[a, tau]=IdFunBw(q,n,B,m,Sb,Y0,L,kh);

%The linearized inertial model
[x, A, Bd, D,Hinit]=LinearInertial(m,B,Y0,n,q,Sb,L,dt,NodeNumGiven,ModelType);

%Hayami
[aa, bb cc dd ee tauhay, cl, yn]=FunHayami(q,n,B,m,Sb,Y0,L,kh,dt);


SimLength=floor(EndTime/dt)+1;
%Note: you calculate the first water level not in the very end of the canal
%in order to follow the other type of discretization (for the SV and Lin
%In)
InitializationTime=max([max(tauIDdownMD) max(tuMD) max(tauIDupMD) max(tdMD) max(tauhay)])+100;

%InitializationTime=floor(max([tau tu td])/dt)+1;
Factor=3600;

%ID model
taud=floor(td/dt);
tauidzup=floor(tu/dt);
taudid=floor(tau/dt);

StartTime=InitializationTime;
hidz(1:StartTime)=0;
hid(1:StartTime)=0;
%hhay(1:StartTime)=0;


hidzup(1:StartTime)=0;
hidup(1:StartTime)=0;

hidzmid(1:StartTime)=0;
hidmid(1:StartTime)=0;

q=ones(SimLength+InitializationTime+2,1)*0;
q(1:InitializationTime+StepFinish,1)=DischargeStep;
q(1:InitializationTime)=0;

qd=ones(SimLength+InitializationTime+2,1)*0;
qd(1:InitializationTime)=0;

% SV
[ Time,Hm2 , Hm,~] = InitialiseFiniteVolume(L, zm,B,n,Sb,EndTime,dt,q+NominalDischarge,qd+NominalDischarge,wm ,NodeNumGiven);
[ Time_,Hm2_ , Hm_,~] = FiniteVoumeModel11bck(L, zm,B,n,Sb,EndTime,dt,q+NominalDischarge,qd+NominalDischarge,wm ,NodeNumGiven,UseInitialization, HinitGiven);

hmid=ones(SimLength+InitializationTime+2,NumberHPoints)*0;
hmidz=ones(SimLength+InitializationTime+2,NumberHPoints)*0;
hhay=ones(SimLength+InitializationTime+2,NumberHPoints)*0;
HinitH=Hm2(1,:);
auM=puma;
p12_infM=pdm;
p21_infM=pum;
%The approximation of the downstream water level
auM=puma_;
tuMD=tuMD_;
tdMD=tdMD_;
hmidz=ones(SimLength+InitializationTime+2,NodeNumGiven)*0;


for k=StartTime:SimLength+InitializationTime+1
    for Nodes=1:NodeNumGiven
      hmidz(k+1,Nodes)=hmidz(k,Nodes)+dt*q(k-tdMD(Nodes))/auM(Nodes)+pum_(Nodes)*(q(k+1-tdMD(Nodes))-q(k-tdMD(Nodes)))...
                      -dt*qd(k-tuMD(Nodes))/auM(Nodes)-pdm_(Nodes)*(qd(k+1-tuMD(Nodes))-qd(k-tuMD(Nodes))); %This should be adm, it it is calculated at all...
   end
end
toc

%%

% 
% %hmidzf=hmidz(1+InitializationTime:SimLength+InitializationTime,:);
% %hmidf=hmid(1+InitializationTime:SimLength+InitializationTime,:);

 hmidzf=hmidz(1:SimLength,:); %changed 0404

%Plot parameters
MyFontSize=12;
MyFont='Arial';
MyGrey=[1 1 1]*0.6;

f=figure(1);
Location=NodeNumGiven;
t=0:dt:(SimLength-1)*dt;
InitialProfile=HinitH;
plot(t/Factor,hmidzf(:,Location)+InitialProfile(Location),'Color',MyGrey,'LineWidth',2)
hold on
%plot(t/Factor,hmidf(:,end)+InitialProfile(end),'--','Color',MyGrey)
%plot(t/Factor,hm(:,end)+InitialProfile(end),'-','Color','red')
plot(t/Factor,Hm2(1:end,Location),'-','Color','blue')
%plot(t/Factor,hmhay(:,end)+InitialProfile(end),'-','Color','green')
title('Downstream')
print(f,'downstream','-dpng','-r300')


f=figure(2);
Location=1;
t=0:dt:(SimLength-1)*dt;
InitialProfile=HinitH;
plot(t/Factor,hmidzf(:,Location)+InitialProfile(Location),'Color',MyGrey,'LineWidth',2)
hold on
%plot(t/Factor,hmidf(:,end)+InitialProfile(end),'--','Color',MyGrey)
%plot(t/Factor,hm(:,end)+InitialProfile(end),'-','Color','red')
plot(t/Factor,Hm2(1:end,Location),'-','Color','blue')
%plot(t/Factor,hmhay(:,end)+InitialProfile(end),'-','Color','green')
title('Upstream')
print(f,'upwnstream','-dpng','-r300')


f=figure(3);
Location=15;
t=0:dt:(SimLength-1)*dt;
InitialProfile=HinitH;
plot(t/Factor,hmidzf(:,Location)+InitialProfile(Location),'Color',MyGrey,'LineWidth',2)
hold on
%plot(t/Factor,hmidf(:,end)+InitialProfile(end),'--','Color',MyGrey)
%plot(t/Factor,hm(:,end)+InitialProfile(end),'-','Color','red')
plot(t/Factor,Hm2(1:end,Location),'-','Color','blue')
%plot(t/Factor,hmhay(:,end)+InitialProfile(end),'-','Color','green')
title('Middle')
print(f,'middle','-dpng','-r300')

