IDZ Distributed version
=======================
The distributed version of the IDZ model is based on Litrico, Xavier, and Vincent Fromion. "Analytical approximation of open-channel flow for controller design." Applied Mathematical Modelling 28.7 (2004): 677-695.


Testing
-------



How to use
----------

It is a Matlab function, that can be called with the following code::

[tuM,tdM,pum,pdm,puma]=IdzFunDist(q,n,B,m,Sb,Y0,L, NodeNumGiven)

where n is the Manning's coefficient, q is the discharge, B is the bottom width, Sb is the bottom slope, Y0 is the downstream water depth and L is the length of the channel. NodeNumGiven is the number of spatial discreatisation nodes. 
The last optional argument is used to change the space discretization. If it is 1, then the same discreatization as for the SV is used.

Implementation
--------------

Expressions are derived for backwater area, zeros and the delay for any given point in the reach based on Litrico, Xavier, and Vincent Fromion. "Analytical approximation of open-channel flow for controller design." Applied Mathematical Modelling 28.7 (2004): 677-695.





