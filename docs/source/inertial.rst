Linear inertial model
=====================


The linear inertial model is the linearized version of the inertial wave equation. That is the
Sainv-Venant equations without the convective term. The equation is described (including discertization) for example in 
Montero, R. A., Schwanenberg, D., Hatz, M., & Brinkmann, M. (2013). Simplified hydraulic modelling in model predictive control of flood mitigation measures along rivers. Journal of Applied Water Engineering and Research, 1(1), 17-27.



How to use
----------

It is a Matlab function, that can be called with the following code::

   [x A Bd D,Hinit]=LinearInertial(z,b,h0,n,q0,s,l,dt,NodeNumGiven,ModelType)

where n is the Manning's coefficient, q is the discharge, B is the bottom width, Sb is the bottom slope, Y0 is the downstream water depth and L is the length of the channel. dt is the discretization time and NodeNumGiven is the number of space nodes used for discretization. ModelType has not function for the time being.


Example
-------

See the file TestLineraInertial.m



