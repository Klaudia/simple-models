Integrator resonance model
==========================

The IR model is implemented based on the paper of Van Overloop, P. J., et al. "Identification of resonance waves in open water channels." Control Engineering Practice 18.8 (2010): 863-872.


Testing
-------


How to use
----------

It is a Matlab function, that can be called with the following code::

[num,den,sysidr,Mr_calc,Om_r_calc, num2, Om_r_calc2]=ident_idr_new(b0 ,h , m ,q ,x,n,Mr,Om_r,UseTravelTime)

where n is the Manning's coefficient, q is the discharge, B is the bottom width, Sb is the bottom slope, Y0 is the downstream water depth and L is the length of the channel.   

Implementation
--------------



Background
----------

The IR model is deduced from the linarized Saint-Venant equations. It is a second order wave with an integrator.
Thus, it has three free parameters: 
- integrator gain (backwater area)
- frequency (inverse of a wave travel time)
- damping

The first parameter determines its behaviour reaching steady state. For open water channels this can be approximated with the backwater area.
Changing this parameter would change the final steady state (and hence the mass balance).
The second parameter is the frequency. This has also very clear physical meaning, and it can be very well approximated by using the travel time of a wave.
The third parameter, the damping is the most difficult to approximate, because the formula (1/(2*Om_r*A*Mr)) contains the resonance peak.
This can be approximated by formulas, but it is not correct. 

For the CF reach, the problem is that the wave is not "high" enough. It can be higher by decreasing the damping. However, even using very small
damping (1e-50), the wave height hardly increases. 

Thus, there is not much possibility to tune this structure more. The first two parameters are set to the best (the backwater area is good, that can be seen in the realistic simulation ),
and now also the resonance frequency is well calculated. (I do suggest to use the formula instead of tuning). The Mr can be a bit changed, but the situation will hardly improve.

Reasoning
^^^^^^^^^

The IR is not meant to be a model for time domain simulations. It is meant to be for control purposes. And also, sometimes I am not convinced how good it is. In time domain, it will never be better as 
our results now. The question is what is your final goal.





